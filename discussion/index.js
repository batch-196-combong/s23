console.log('Hello Dave');

// JS Objects
// is a way to define a real world object with its own characteristics.
// a way to organize data with context through the use of key value pairs. 


let movie = {
	title: 'Avengers',
	publisher: 'Kevin Feige',
	year: '2019',
	price: 200,
	isAvailable: true
}

console.log(movie);
// Key-Value - properties of objects
/* object = {
	key:value
}*/

// Accessing array items = arrName[index]
// Accessing object properties = objectName.propertyName
console.log(movie.title);

// update property
movie.price = 300;
console.log(movie);

let videoGame = movie;
console.log(videoGame);

videoGame.title = 'Final Fantasy';
videoGame.publisher = 'Square Enix';
videoGame.year = '2001';

console.log(videoGame);

let course = {
	title: 'philosophy 101',
	description: 'Learn the values of life',
	price: 5000, 
	isActive: true,
	instructors: ['Mr. Johnson', 'Mrs. Smith', 'Mr. Francis']
}

console.log(course);
console.log(course.instructors);

// access array element inside object
console.log(course.instructors[1]);
let lastInstructor = course.instructors.pop();
console.log(course.instructors);
console.log(lastInstructor);

course.instructors.push('Mr. McGee');
console.log(course.instructors);

let arrayMethod = course.instructors.includes('Mr. Johnson');
console.log(arrayMethod);

function addNewInstructor(instructor){

	if(course.instructors.includes(instructor)){
		console.log('Instructor already added');
	} else {
		course.instructors.push(instructor);
		console.log('Thank You. Instructor added')
		
	}

}

// addNewInstructor('Mr. Marco');
// addNewInstructor('Mr. Smith');

// console.log(course);

let instructor = {};
instructor.name = 'James Johnsons'
instructor.age = 56;
instructor.gender = 'Male';
instructor.department = 'Humanities';
instructor.salary = 50000;
instructor.subject = ['Philosophy', 'Humanities', 'Logic'];
console.log(instructor);

instructor.address = {
	street: '#1 Maginhawa St.',
	city: 'Quezon City',
	country: 'Philippines'
}
console.log(instructor);

console.log(instructor.address.street);

function Superhero(name, superpower, powerLevel){
	this.name = name;
	this.superpower = superpower;
	this.powerLevel = powerLevel;
}

let superhero1 = new Superhero ('Saitama', 'One Punch', 30000);
console.log(superhero1);

function Laptop(name, brand, price){
	this.name = name;
	this.brand = brand;
	this.price = price;
}

let laptop1 = new Laptop ('Nitro 5', 'Acer', 50000);
let laptop2 = new Laptop ('Legion 5', 'Lenovo', 55000);
console.log(laptop1);
console.log(laptop2);

// Object Methods
// function is a property a function is associated with an object
// methods are tasks that an object can perform

let person = {
	name: 'Slim Body',
	talk: function(){
		console.log(this)
		console.log('Hi My name is, What? My name is who? ' + this.name)
	}
}

let person2 = {
	name: 'Dr. Dre',
	greet: function(friend){
		console.log('Good Day ' + friend.name);
	}
}
person.talk();

person2.greet(person);

function Dog(name, breed){
	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log('Bark! bark ' + friend.name);
	}
}

let dog1 = new Dog('Brown','Belgian');
console.log(dog1);
dog1.greet(person);
dog1.greet(person2);

let dog2 = new Dog('Blackie', 'Rotweiler');
console.log(dog2);

dog2.greet(dog1);