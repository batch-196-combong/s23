console.log('Hello David');

let trainer = {
	name: 'Ash Ketchum',
	age: 22,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: ['Brock','Misty'],
	talk: function(){
		console.log(this.pokemon[0] + '! I chose you');
	}
}

console.log(trainer);
console.log('Result of Dot Notation: ');
console.log(trainer.name);
console.log('Result of square bracket notation: ');
console.log(trainer.pokemon);
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 3*level;
	this.attack = 1.5*level;
	this.tackle = function(opponent){
		console.log(name + ' tackled ' + opponent.name);
		opponent.health = opponent.health-this.attack;
		console.log(opponent.name + '\'s health' + ' is now ' + opponent.health);
		if(opponent.health <= 0){
			opponent.faint();
	}
		console.log(opponent);
		}
		this.faint = function(){
		console.log(name + ' has fainted');
	}
}

let pokemon1 = new Pokemon('Pikachu', 12);
console.log(pokemon1);
let pokemon2 = new Pokemon('Geodude', 8);
console.log(pokemon2);
let pokemon3 = new Pokemon('Mewto', 100);
console.log(pokemon3);

pokemon2.tackle(pokemon1);
pokemon3.tackle(pokemon2);

